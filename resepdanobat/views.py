from django.shortcuts import render, redirect
from django.db import IntegrityError
from .db import ResepdanObat
from authentication.auth import login_required, admin_permission


'''View for Buat Resep'''
@login_required
@admin_permission
def buat_resep(request):
    if request.method == 'POST':
        id_konsultasi = request.POST['id_konsultasi']
        id_transaksi = request.POST['id_transaksi']
        daftar_kode_obat = request.POST.getlist('daftar_kode_obat')
        ResepdanObat().buat_resep(id_konsultasi, id_transaksi, daftar_kode_obat)
        return redirect("resepdanobat:daftar_resep")
    else:
        list_id_konsultasi = ResepdanObat().get_id_konsultasi()
        list_id_transaksi = ResepdanObat().get_id_transaksi()
        list_kode_obat = ResepdanObat().get_kode_obat()
        return render(request, 'buat-resep.html', {'list_id_konsultasi':list_id_konsultasi, 'list_id_transaksi':list_id_transaksi, 'list_kode_obat':list_kode_obat})

'''View for Daftar Resep'''
@login_required
def daftar_resep(request):
    daftar_resep_dan_obat = ResepdanObat().get_daftar_resep()
    return render(request, 'daftar-resep.html', {'daftar_resep_dan_obat':daftar_resep_dan_obat})

'''View for Delete Resep'''
@login_required
@admin_permission
def delete_resep(request, no_resep):
    if request.method == 'POST':
        ResepdanObat().delete_resep(no_resep)
    return redirect("resepdanobat:daftar_resep")

'''View for Buat Obat'''
@login_required
@admin_permission
def buat_obat(request):
    if request.method == 'POST':
        try:
            kode = request.POST['kode']
            stock = request.POST['stock']
            harga = request.POST['harga']
            komposisi = request.POST['komposisi']
            bentuk = request.POST['bentuk']
            merek = request.POST['merek']
            ResepdanObat().buat_obat(kode, stock, harga, komposisi, bentuk, merek)
            return redirect("resepdanobat:daftar_obat")
        except IntegrityError as e:
            return render(request, 'buat-obat.html', {'error':True})
    else:
        return render(request, 'buat-obat.html')

'''Views for Daftar Obat'''
@login_required
def daftar_obat(request):
    daftar_obat = ResepdanObat().get_daftar_obat()
    return render(request, 'daftar-obat.html', {'daftar_obat':daftar_obat})

'''Views to Update Obat'''
@login_required
@admin_permission
def update_obat(request, kode):
    if request.method == 'POST':
        stok = request.POST['stok']
        harga = request.POST['harga']
        komposisi = request.POST['komposisi']
        bentuk_sediaan = request.POST['bentuk_sediaan']
        merk_dagang = request.POST['merk_dagang']
        ResepdanObat().update_obat(kode, stok, harga, komposisi, bentuk_sediaan, merk_dagang)
        return redirect("resepdanobat:daftar_obat")
    else:
        info_obat = ResepdanObat().get_info_obat(kode)
        return render(request, 'update-obat.html', {'info_obat':info_obat})

'''Views to Delete Obat'''
@login_required
@admin_permission
def delete_obat(request, kode):
    if request.method == 'POST':
        kode = int(kode)
        ResepdanObat().delete_obat(kode)
    return redirect("resepdanobat:daftar_obat")