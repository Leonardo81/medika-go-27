from django.db import connection
from medikago.serializers import serialize
import random

class ResepdanObat():
    cursor = None

    def __init__(self):
        self.cursor = connection.cursor()
        self.cursor.execute("SET SEARCH_PATH TO medika_go")

    """
    Function for buat_resep
    """
    def get_id_konsultasi(self):
        self.cursor.execute("SELECT id_konsultasi FROM SESI_KONSULTASI")
        return serialize(self.cursor)
    
    def get_id_transaksi(self):
        self.cursor.execute("SELECT id_transaksi FROM TRANSAKSI")
        return serialize(self.cursor)

    def get_kode_obat(self):
        self.cursor.execute("SELECT kode, merk_dagang FROM OBAT")
        return serialize(self.cursor)
    
    def max_no_resep(self):
        self.cursor.execute("SELECT MAX(no_resep::int) FROM resep")
        return serialize(self.cursor)

    def buat_resep(self, id_konsultasi, id_transaksi, daftar_kode_obat):
        rand_dosis = ['overdosis', '1 tablet sehari', '3 tablet sehari', 'jangan dimakan']
        rand_aturan = ['setelah makan', 'atur sendiri', 'gaada aturan', 'sebelum berhubungan']
        
        set_kode_obat = set(daftar_kode_obat)
        
        self.cursor.execute("INSERT INTO RESEP(id_konsultasi, no_resep, total_harga, id_transaksi) VALUES('{}', '{}', '{}', '{}')".format(id_konsultasi, 0, 0, id_transaksi))
        no_resep = (self.max_no_resep()[0]).get("max")


        for kode_obat in daftar_kode_obat:
            dosis = random.choice(rand_dosis)
            aturan = random.choice(rand_aturan)
            self.cursor.execute("INSERT INTO DAFTAR_OBAT(no_resep, kode_obat, dosis, aturan) VALUES ('{}', '{}', '{}', '{}')".format(no_resep, kode_obat, dosis, aturan))
        
        self.cursor.execute("UPDATE RESEP SET total_harga = 270 WHERE no_resep = {}::VARCHAR(50)".format(no_resep))

    """
    Function for daftar_resep
    """
    def get_daftar_resep(self):
        self.cursor.execute("SELECT no_resep, total_harga, id_konsultasi, id_transaksi, string_agg(merk_dagang, ', ') as kodeobat FROM RESEP NATURAL JOIN DAFTAR_OBAT JOIN OBAT ON daftar_obat.kode_obat = obat.kode GROUP BY no_resep")
        return serialize(self.cursor)

    def delete_resep(self, no_resep):
        self.cursor.execute("DELETE FROM DAFTAR_OBAT WHERE no_resep='{}'".format(no_resep))
        self.cursor.execute("DELETE FROM RESEP WHERE no_resep='{}'".format(no_resep))

    """
    Function for buat_obat
    """
    def buat_obat(self, kode, stock, harga, komposisi, bentuk, merek):
        self.cursor.execute("INSERT INTO OBAT (kode, stok, harga, komposisi, bentuk_sediaan, merk_dagang) VALUES ('{}', {}, {}, '{}', '{}', '{}')".format(kode, stock, harga, komposisi, bentuk, merek))
    
    """
    Function for daftar_obat
    """
    def get_daftar_obat(self):
        self.cursor.execute("SELECT * FROM OBAT")
        return serialize(self.cursor)

    def get_info_obat(self, kode):
        self.cursor.execute("SELECT * FROM OBAT WHERE kode='{}'".format(kode))
        return serialize(self.cursor)[0]

    def update_obat(self, kode, stock, harga, komposisi, bentuk_sediaan, merk_dagang):
        self.cursor.execute("UPDATE OBAT SET stok={}, harga={}, komposisi='{}', bentuk_sediaan='{}', merk_dagang='{}' WHERE kode='{}'".format(stock, harga, komposisi, bentuk_sediaan, merk_dagang, kode))

    def delete_obat(self, kode):
        self.cursor.execute("DELETE FROM DAFTAR_OBAT WHERE kode_obat='{}'".format(kode))
        self.cursor.execute("DELETE FROM OBAT WHERE kode='{}'".format(kode))