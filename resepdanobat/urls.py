from django.urls import path
from . import views

app_name='resepdanobat'

urlpatterns = [
    path('buat-resep/', views.buat_resep, name="buat_resep"),
    path('daftar-resep/', views.daftar_resep, name="daftar_resep"),
    path('daftar-resep/delete-resep/<int:no_resep>/', views.delete_resep, name="delete_resep"),
    path('buat-obat/', views.buat_obat, name="buat_obat"),
    path('daftar-obat/', views.daftar_obat, name="daftar_obat"),
    path('daftar-obat/<str:kode>/', views.update_obat, name="update_obat"),
    path('daftar-obat/delete-obat/<int:kode>/', views.delete_obat, name="delete_obat"),
]
