from django.db import connection
from medikago.serializers import serialize

class Poliklinik():
    cursor = None

    def __init__(self):
        self.cursor = connection.cursor()
        self.cursor.execute("SET SEARCH_PATH TO medika_go")

    def get_all_poliklinik(self):
        self.cursor.execute("SELECT * FROM LAYANAN_POLIKLINIK ORDER BY id_poliklinik")
        return serialize(self.cursor)

    '''Query for getting all id_poliklinik'''
    def get_all_id_poliklinik(self):
        self.cursor.execute("SELECT DISTINCT id_poliklinik, nama FROM LAYANAN_POLIKLINIK ORDER BY id_poliklinik")
        return serialize(self.cursor)

    def get_new_id_poliklinik(self):
        self.cursor.execute("SELECT coalesce(max(id_poliklinik::INT), 0) as max FROM LAYANAN_POLIKLINIK")
        max_id = int(serialize(self.cursor)[0]['max'])
        return max_id+1

    def create_new_poliklinik(self, id, nama, desc, kode_rs):
        self.cursor.execute(
            "INSERT INTO LAYANAN_POLIKLINIK (id_poliklinik, kode_rs_cabang, nama, deskripsi) VALUES ('{}','{}','{}','{}')"
            .format(id, kode_rs, nama, desc)
        )

    def create_layanan_with_jadwal(self, nama, desc, kode_rs, jadwal):
        id = self.get_new_id_poliklinik()
        self.create_new_poliklinik(id, nama, desc, kode_rs)
        for i in range(len(jadwal['hari'])):
            id_jadwal = JadwalPoli().get_new_id_jadwal()
            JadwalPoli().create_new_jadwal(id_jadwal, jadwal['hari'][i], jadwal['mulai'][i], jadwal['selesai'][i], jadwal['kapasitas'][i], jadwal['dokter'][i], id)

    '''View for getting layanan poli detail by id_poliklinik'''
    def get_detail_layanan_by_id(self, id_poli):
        self.cursor.execute(
            "SELECT * FROM LAYANAN_POLIKLINIK WHERE id_poliklinik='{}'".format(id_poli)
        )
        return serialize(self.cursor)

    '''View for updating layanan poliklinik details'''
    def update_poliklinik(self, id_poli, kode_rs, nama_layanan, deskripsi):
        self.cursor.execute(
            "UPDATE LAYANAN_POLIKLINIK SET kode_rs_cabang='{}', nama='{}', deskripsi='{}' WHERE id_poliklinik='{}'".format(kode_rs,nama_layanan,deskripsi,id_poli)
        ) 

    def delete(self, id):
        self.cursor.execute("DELETE FROM LAYANAN_POLIKLINIK WHERE id_poliklinik='{}'".format(id))
    

class JadwalPoli():
    cursor = None
    
    def __init__(self):
        self.cursor = connection.cursor()
        self.cursor.execute("SET SEARCH_PATH TO medika_go")

    def get_all_jadwal(self):
        self.cursor.execute("SELECT * FROM JADWAL_LAYANAN_POLIKLINIK ORDER BY (id_jadwal_poliklinik::int)")
        return serialize(self.cursor)

    def get_new_id_jadwal(self):
        self.cursor.execute("SELECT coalesce(max(id_jadwal_poliklinik::INT), 0) as max FROM JADWAL_LAYANAN_POLIKLINIK")
        max_id = int(serialize(self.cursor)[0]['max'])
        return max_id+1

    '''Query for creating Jadwal Layanan Poliklinik'''
    def create_new_jadwal(self, id, hari, mulai, selesai, kapasitas, id_dokter, id_poliklinik):
        self.cursor.execute(
            "INSERT INTO JADWAL_LAYANAN_POLIKLINIK (id_jadwal_poliklinik, hari, waktu_mulai, waktu_selesai, kapasitas, id_dokter, id_poliklinik) VALUES ('{}','{}','{}','{}',{},'{}','{}')"
            .format(id, hari, mulai, selesai, kapasitas, id_dokter, id_poliklinik)
        )
    
    '''Query for getting jadwal Layanan Poli terkait'''
    def get_related_jadwal(self, id_poli):
        self.cursor.execute(
            "SELECT * FROM JADWAL_LAYANAN_POLIKLINIK WHERE id_poliklinik='{}' ORDER BY id_jadwal_poliklinik".format(id_poli)
        )
        return serialize(self.cursor)

    '''Query for getting detail jadwal layanan poliklinik terkait by id'''
    def get_detail_jadwal_by_id(self,id_jadwal_poliklinik):
        self.cursor.execute(
            "SELECT * FROM JADWAL_LAYANAN_POLIKLINIK WHERE id_jadwal_poliklinik='{}'".format(id_jadwal_poliklinik)
        )
        return serialize(self.cursor)

    '''Query for updating jadwal layanan poliklinik'''
    def update_jadwal_poliklinik(self, id_jadwal_poliklinik, hari, waktu_mulai, waktu_selesai, kapasitas, id_dokter, id_poliklinik):
        self.cursor.execute(
            "UPDATE JADWAL_LAYANAN_POLIKLINIK SET waktu_mulai='{}', waktu_selesai='{}', hari='{}', kapasitas='{}', id_dokter='{}' WHERE id_jadwal_poliklinik='{}' AND id_poliklinik='{}'"
            .format(waktu_mulai, waktu_selesai, hari, kapasitas, id_dokter, id_jadwal_poliklinik, id_poliklinik)
        )
    
    '''Query for deleting jadwal layanan poliklinik by id'''
    def delete_jadwal_by_id(self, id_jadwal_poliklinik):
        self.cursor.execute(
            "DELETE FROM JADWAL_LAYANAN_POLIKLINIK WHERE id_jadwal_poliklinik='{}'".format(id_jadwal_poliklinik)
        )