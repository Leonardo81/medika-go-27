from django.shortcuts import redirect, render
from .db import Poliklinik, JadwalPoli
from rscabang.db import RSCabang
from authentication.auth import login_required, admin_permission

'''View for Buat Layanan Poliklinik Menu'''
@login_required
@admin_permission
def buat_layananPoli(request):
    error = False
    if request.method == 'POST':
        nama = request.POST['nama']
        deskripsi = request.POST['deskripsi']
        kode_rs = request.POST['kode_rs']
        jadwal = {
            'hari' : request.POST.getlist('hari'),
            'mulai' : request.POST.getlist('jam_mulai'),
            'selesai' : request.POST.getlist('jam_selesai'),
            'kapasitas' : request.POST.getlist('kapasitas'),
            'dokter': request.POST.getlist('dokter')
        }
        try:
            Poliklinik().create_layanan_with_jadwal(nama, deskripsi, kode_rs, jadwal)
            return redirect('poliklinik:list_layananPoli')
        except:
            error = True
            pass
    list_rs = RSCabang().get_list_daftar_cabang()
    list_dokter = RSCabang().get_all_id_dokter()
    return render(request, 'buat-layananPoli.html', {'list_rs': list_rs, 'list_dokter': list_dokter, "error":error})

'''View for Daftar Layanan Poliklinik Menu'''
@login_required
def list_layananPoli(request):
    poliklinik = Poliklinik().get_all_poliklinik()
    return render(request, 'daftar-layananPoli.html', {'poliklinik': poliklinik})

'''View for Action Update Layanan Poliklinik'''
@login_required
@admin_permission
def update_layananPoli(request, id_poli):
    error= False
    detail_layanan = Poliklinik().get_detail_layanan_by_id(id_poli)[0]
    daftar_rs_cabang = RSCabang().get_all_kode_rs()
    if request.method == "POST":
        id_poliklinik = id_poli
        nama_layanan = request.POST['nama_layanan']
        deskripsi = request.POST['deskripsi']
        kode_rs = request.POST['kode_rs']
        try:
            Poliklinik().update_poliklinik(id_poliklinik,kode_rs,nama_layanan,deskripsi)
            return redirect("poliklinik:list_layananPoli")
        except:
            error = True
            pass
    return render(request, 'update-layananPoli.html', {"detail_layanan":detail_layanan, "daftar_rs_cabang":daftar_rs_cabang, "id_poli":id_poli, "error":error})

'''View for action delete Layanan Poliklinik'''
@login_required
@admin_permission
def delete_layananPoli(request, id_poli):
    if request.method == "POST":
        Poliklinik().delete(id_poli)
    return redirect('poliklinik:list_layananPoli')

'''View for Button Jadwal Layanan on Daftar Layanan Poliklinik'''
@login_required
def jadwal_layananPoli(request, id_poli):
    list_related_jadwal = JadwalPoli().get_related_jadwal(id_poli)
    return render(request, 'jadwal_layananPoli.html', {"list_related_jadwal":list_related_jadwal, "id_poli":id_poli})

'''View for Buat Jadwal Poliklinik Menu'''
@login_required
@admin_permission
def buat_jadwalPoli(request):
    daftar_hari = ["Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"]
    list_id_dokter = RSCabang().get_all_id_dokter()
    list_id_poliklinik = Poliklinik().get_all_id_poliklinik()
    error = False
    if request.method == "POST":
        hari = request.POST['hari']
        waktu_mulai = request.POST['waktu_mulai']
        waktu_selesai = request.POST['waktu_selesai']
        kapasitas = request.POST['kapasitas']
        id_dokter = request.POST['id_dokter']
        id_poliklinik = request.POST['id_poliklinik']
        id_jadwal = JadwalPoli().get_new_id_jadwal()
        try:
            JadwalPoli().create_new_jadwal(id_jadwal, hari, waktu_mulai, waktu_selesai, kapasitas, id_dokter, id_poliklinik)
            return redirect('poliklinik:daftar_jadwalPoli')
        except:
            error = True
            pass
    return render(request, 'buat_jadwalPoli.html', {"daftar_hari":daftar_hari, "list_id_dokter":list_id_dokter, "list_id_poliklinik":list_id_poliklinik, "error":error})

'''View for Daftar Jadwal Poliklinik Menu'''
@login_required
def daftar_jadwalPoli(request):
    list_all_jadwal = JadwalPoli().get_all_jadwal()
    return render(request, 'daftar-jadwalPoli.html', {"list_all_jadwal":list_all_jadwal})

'''View for Action Update Jadwal Poliklinik'''
@login_required
@admin_permission
def update_jadwalPoli(request, id_jadwal):
    error = False
    detail_jadwal = JadwalPoli().get_detail_jadwal_by_id(id_jadwal)[0]
    waktu_mulai_converted = detail_jadwal['waktu_mulai'].isoformat()
    waktu_selesai_converted = detail_jadwal['waktu_selesai'].isoformat()
    detail_jadwal['waktu_mulai'] = waktu_mulai_converted
    detail_jadwal['waktu_selesai'] = waktu_selesai_converted
    daftar_hari = ["Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"]
    list_id_dokter = RSCabang().get_all_id_dokter()
    if request.method == "POST":
        id_jadwal_poliklinik = id_jadwal
        hari = request.POST['hari']
        waktu_mulai = request.POST['waktu_mulai']
        waktu_selesai = request.POST['waktu_selesai']
        kapasitas = request.POST['kapasitas']
        id_dokter = request.POST['id_dokter']
        id_poliklinik = detail_jadwal['id_poliklinik']
        try:
            JadwalPoli().update_jadwal_poliklinik(id_jadwal_poliklinik, hari, waktu_mulai, waktu_selesai, kapasitas, id_dokter, id_poliklinik)
            return redirect("poliklinik:daftar_jadwalPoli")
        except:
            error = True
            pass
    return render(request, 'update-jadwalPoli.html', {"error":error, "detail_jadwal":detail_jadwal, "list_id_dokter":list_id_dokter, "daftar_hari":daftar_hari, "id_jadwal_poliklinik":id_jadwal})

'''View for Deleting Jadwal Poliklinik'''
@login_required
@admin_permission
def delete_jadwalPoli(request, id_jadwal):
    if request.method == "POST":
        JadwalPoli().delete_jadwal_by_id(id_jadwal)
    return redirect('poliklinik:daftar_jadwalPoli')
