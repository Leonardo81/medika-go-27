from django.urls import path
from . import views

app_name='poliklinik'

urlpatterns = [
    path('buat-layanan-poliklinik/', views.buat_layananPoli, name="buat_layananPoli"), #url for buat-layanan poliklinik menu
    path('daftar-layanan-poliklinik/', views.list_layananPoli, name="list_layananPoli"), #url for daftar-layanan-poliklinik menu
    path('daftar-layanan-poliklinik/update/<int:id_poli>', views.update_layananPoli, name="update_layananPoli"), #url for updating layanan poli
    path('daftar-layanan-poliklinik/delete/<int:id_poli>', views.delete_layananPoli, name="delete_layananPoli"),
    path('daftar-layanan-poliklinik/jadwal/<int:id_poli>', views.jadwal_layananPoli, name="jadwal_layananPoli"),  #url for handling button Jadwal Layanan
    path('buat-jadwal-poliklinik/', views.buat_jadwalPoli, name="buat_jadwalPoli"), #url for handling Buat Jadwal Poliklinik
    path('delete-jadwal-poliklinik/<int:id_jadwal>', views.delete_jadwalPoli, name="delete_jadwalPoli"), #url for handling Delete Jadwal Poliklinik
    path('daftar-jadwal-poliklinik/', views.daftar_jadwalPoli, name="daftar_jadwalPoli"),   #url for Daftar Jadwal Poliklinik menu
    path('daftar-jadwal-poliklinik/update/<int:id_jadwal>', views.update_jadwalPoli, name="update_jadwalPoli"),   #url for Action Update Jadwal Poliklinik
]
