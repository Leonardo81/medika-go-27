from django.urls import path
from . import views

app_name='rscabang'
urlpatterns = [
    path('buat-rscabang/', views.buat_rscabang, name='buat_rscabang'),  #untuk menu Buat RS Cabang
    path('daftar-rscabang/', views.daftar_rscabang, name='daftar_rscabang'),    #untuk menu Daftar RS Cabang
    path('daftar-rscabang/<int:koders>', views.update_rscabang, name='update_rscabang'), #untuk action update Daftar RS Cabang
    path('daftar-rscabang/delete/<int:koders>', views.delete_rscabang, name='delete_rscabang'),
    path('add_dokter/', views.add_dokter, name='add_dokter'),    #untuk  Daftarkan Dokter
    path('list_dokter/', views.list_dokter, name='list_dokter'),    #untuk menu Daftar Dokter-RS Cabang
    path('list_dokter/<slug:kode_rs>/<slug:id_dokter>', views.update_dokter, name='update_dokter'), #menu untuk action update di tabel daftar dokter
    path('list_dokter/delete/<slug:kode_rs>/<slug:id_dokter>', views.delete_pair, name='delete_pair'),  #menu untuk delete pair RS Cabang-Dokter RS Cabang
    path('daftarkan-administrator/', views.daftarkan_administrator, name='daftarkan_administrator'),    #untuk menu Daftarkan Administrator
]