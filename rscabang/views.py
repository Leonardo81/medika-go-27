from django.shortcuts import render, redirect
from .db import RSCabang, Admin
from authentication.auth import login_required, admin_permission

'''View for Buat RS Cabang menu'''
@login_required
@admin_permission
def buat_rscabang(request):
    error = False
    if request.method == 'POST':
        kode = request.POST['kode']
        nama = request.POST['nama']
        tanggal = request.POST['tanggal']
        jalan = request.POST['jalan']
        nomor = request.POST['nomor']
        kota = request.POST['kota']
        try: 
            RSCabang().create_rs_cabang(kode, nama, tanggal, jalan, nomor, kota)
            return redirect("rscabang:daftar_rscabang")
        except:
            error = True
    kode_terakhir = RSCabang().get_last_koder()
    kode = kode_terakhir[0]['max'] +1
    return render(request, 'buat-rscabang.html', {'kode':kode, 'error': error})

'''View for Daftar RS Cabang menu'''
@login_required
def daftar_rscabang(request):
    list_rs_cabang = RSCabang().get_list_daftar_cabang()
    return render(request, 'daftar-rscabang.html', {'list_rscabang':list_rs_cabang})

'''View for updating data RS Cabang'''
@login_required
@admin_permission
def update_rscabang(request, koders):
    error = False
    rs = RSCabang().get_detailed_rs(koders)
    tanggal = str(rs[0]['tanggal_pendirian'])
    if request.method == 'POST':
        kode = koders
        nama = request.POST['nama']
        tanggal = request.POST['tanggal']
        jalan = request.POST['jalan']
        nomor = request.POST['nomor']
        kota = request.POST['kota']
        try:
            RSCabang().update_detailed_rs(kode, nama, tanggal, jalan, nomor, kota)
            return redirect("rscabang:daftar_rscabang")
        except:
            error = True
            pass
    return render(request, 'update_rscabang.html', {'rs':rs[0], 'tanggal':tanggal, "error":error})

@login_required
@admin_permission
def delete_rscabang(request, koders):
    if request.method == 'POST':
        RSCabang().delete(koders)
    return redirect("rscabang:daftar_rscabang")

'''Views for Daftarkan Dokter'''
@login_required
@admin_permission
def add_dokter(request):
    error=False
    if request.method == 'POST':
        id_dokter = request.POST['id_dokter']
        kode_rs = request.POST['kode_rs']
        try:
            RSCabang().add_dokter_rs_cabang(id_dokter, kode_rs)
            return redirect("rscabang:list_dokter")
        except:
            error=True
            pass
    list_id_dokter= RSCabang().get_all_id_dokter()
    list_kode_rs = RSCabang().get_all_kode_rs()
    return render(request, 'add_dokter.html', {"error":error,"list_id_dokter":list_id_dokter, "list_kode_rs":list_kode_rs})

'''Views for outputing all dokter-RS Cabang'''
@login_required
def list_dokter(request):
    list_pair_items = RSCabang().get_all_pair_kodeRS_idDokter()
    list_kodeRS = RSCabang().get_all_kodeRS_from_pair()
    return render(request, 'list_dokter.html', {"list_pair_items":list_pair_items, "list_kodeRS":list_kodeRS})

'''View for Updating data dokter-RS Cabang'''
@login_required
@admin_permission
def update_dokter(request, kode_rs, id_dokter):
    error = False
    old_kode_rs = kode_rs
    old_id_dokter = id_dokter
    detail_old_dokter = RSCabang().get_detail_dokter_by_id(id_dokter)
    detail_old_rs = RSCabang().get_detail_rs_by_id(kode_rs)
    list_kodeRS = RSCabang().get_all_kode_rs()
    list_idDokter = RSCabang().get_all_id_dokter()
    if request.method=="POST":
        new_kode_rs = request.POST['kode_rs']
        new_id_dokter = request.POST['id_dokter']
        try:
            RSCabang().update_pair(old_kode_rs, old_id_dokter, new_kode_rs, new_id_dokter)
            return redirect("rscabang:list_dokter")
        except:
            error = True
            pass 
    return render(
        request, 'update-dokter.html',
        {"old_kode_rs":old_kode_rs, "old_id_dokter":old_id_dokter, "list_kodeRS":list_kodeRS, "list_idDokter":list_idDokter, "detail_old_dokter":detail_old_dokter,
        "detail_old_rs":detail_old_rs, "error":error}
    )

'''View for Deleting pair Dokter-RS Cabang'''
@login_required
@admin_permission
def delete_pair(request, kode_rs, id_dokter):
    if request.method == "POST":
        RSCabang().delete_pair(kode_rs, id_dokter)
    return redirect('rscabang:list_dokter')

'''View for Daftarkan Administrator menu'''
@login_required
@admin_permission
def daftarkan_administrator(request):
    error = False
    if request.method == "POST":
        no_pegawai = request.POST['nomor_pegawai']
        kode_rs = request.POST['kode_rs']
        try:
            Admin().assign_new_rs(no_pegawai, kode_rs)
            return redirect('rscabang:daftar_rscabang')
        except:
            error = True
    list_rs_cabang = RSCabang().get_list_daftar_cabang()
    list_admin = Admin().get_all_nomor_pegawai()
    response = {
        'list_rs_cabang' : list_rs_cabang,
        'list_admin' : list_admin,
        'error' : error
    }
    return render(request, 'daftarkan-administrator.html', response)
