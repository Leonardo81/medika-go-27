from django.db import connection
from medikago.serializers import serialize

class RSCabang():
    cursor = None

    def __init__(self):
        self.cursor = connection.cursor()
        self.cursor.execute("SET SEARCH_PATH TO medika_go")

    def get_last_koder(self):
        self.cursor.execute("SELECT MAX(kode_rs::int) FROM RS_CABANG")
        return serialize(self.cursor)
    
    def create_rs_cabang(self, kode, nama, tanggal, jalan, nomor, kota):
        self.cursor.execute("INSERT INTO RS_CABANG(kode_rs, nama, tanggal_pendirian, jalan, kota, nomor) VALUES ('{}', '{}', '{}', '{}', '{}', {})".format(kode, nama, tanggal, jalan, kota, nomor))

    def get_list_daftar_cabang(self):
        self.cursor.execute("SELECT * FROM RS_CABANG")
        return serialize(self.cursor)

    def get_detailed_rs(self, koders):
        self.cursor.execute("SELECT * FROM RS_CABANG WHERE kode_rs = '{}' ".format(koders))
        return serialize(self.cursor)

    def update_detailed_rs(self, kode, nama, tanggal, jalan, nomor, kota):
        self.cursor.execute("UPDATE RS_CABANG SET nama = '{}', tanggal_pendirian='{}', jalan = '{}', nomor={}, kota='{}' WHERE kode_rs = '{}'".format( nama, tanggal, jalan, nomor, kota, kode))

    def delete(self, koders):
        self.cursor.execute("DELETE FROM RS_CABANG WHERE kode_rs='{}'".format(koders))

    '''Query for getting all ID Dokter'''
    def get_all_id_dokter(self):
        self.cursor.execute("SELECT id_dokter, username FROM DOKTER")
        return serialize(self.cursor)

    '''Query for getting detail Dokter from id_dokter'''
    def get_detail_dokter_by_id(self, id_dokter):
        self.cursor.execute("SELECT * from DOKTER WHERE id_dokter='{}'".format(id_dokter))
        return serialize(self.cursor)[0]

    '''Query for getting detail RS Cabang by kode_rs'''
    def get_detail_rs_by_id(self, kode_rs):
        self.cursor.execute("SELECT * from RS_CABANG WHERE kode_rs='{}'".format(kode_rs))
        return serialize(self.cursor)[0]

    '''Query for getting all Kode RS'''
    def get_all_kode_rs(self):
        self.cursor.execute("SELECT kode_rs, nama FROM RS_CABANG ORDER BY kode_rs")
        return serialize(self.cursor)

    '''Query for daftarkan dokter RS Cabang'''
    def add_dokter_rs_cabang(self, id_dokter, kode_rs):
        self.cursor.execute(
            "INSERT INTO DOKTER_RS_CABANG(id_dokter,kode_rs) VALUES ('{}', '{}')".format(id_dokter, kode_rs)
        )

    '''Query for getting all pair of kode_rs - id_dokter'''
    def get_all_pair_kodeRS_idDokter(self):
        self.cursor.execute(
            "SELECT kode_rs, id_dokter FROM DOKTER_RS_CABANG ORDER BY kode_rs"
        )
        return serialize(self.cursor)
    
    '''Query for getting the quantity of kode RS available'''
    def get_total_kodeRS(self):
        self.cursor.execute(
            "SELECT COUNT(kode_rs) FROM (SELECT DISTINCT kode_rs FROM DOKTER_RS_CABANG) as k"
        )
        return serialize(self.cursor)

    '''Query for getting all the kode RS available from pairs'''
    def get_all_kodeRS_from_pair(self):
        self.cursor.execute(
            "SELECT DISTINCT kode_rs FROM DOKTER_RS_CABANG ORDER BY kode_rs"
        )
        return serialize(self.cursor)

    '''Query for getting all the id Dokter available from pairs'''
    def get_all_idDokter_from_pair(self):
        self.cursor.execute(
            "SELECT DISTINCT (id_dokter::int) FROM DOKTER_RS_CABANG ORDER BY (id_dokter::int)"
        )
        return serialize(self.cursor)

    '''Query for updating RS Cabang-Dokter pair'''
    def update_pair(self, old_kode_rs, old_id_dokter, new_kode_rs, new_id_dokter):
        self.cursor.execute(
            "UPDATE DOKTER_RS_CABANG SET kode_rs='{}', id_dokter='{}' WHERE kode_rs='{}' AND id_dokter='{}'".format(new_kode_rs,new_id_dokter,old_kode_rs,old_id_dokter)
        )

    '''Query for deleting Dokter-RS Cabang pair'''
    def delete_pair(self, kode_rs, id_dokter):
        self.cursor.execute(
            "DELETE FROM DOKTER_RS_CABANG WHERE kode_rs='{}' AND id_dokter='{}'".format(kode_rs,id_dokter)
        )
class Admin():
    cursor = None

    def __init__(self):
        self.cursor = connection.cursor()
        self.cursor.execute("SET SEARCH_PATH TO medika_go")

    def get_all_nomor_pegawai(self):
        self.cursor.execute("SELECT nomor_pegawai, username FROM ADMINISTRATOR")
        return serialize(self.cursor)

    def assign_new_rs(self, nomor_pegawai, kode_rs):
        self.cursor.execute(
            "UPDATE ADMINISTRATOR SET kode_rs='{}' WHERE nomor_pegawai='{}'"
            .format(kode_rs, nomor_pegawai)
        ) 