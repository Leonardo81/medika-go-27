from django.shortcuts import render, redirect
from authentication.auth import login_required
from .db import Pengguna

def login(request):
  if request.method == 'POST':
    username = request.POST['username']
    password = request.POST['password']
    pengguna = Pengguna().get_user(username, password)
    if pengguna != None:
      is_admin = Pengguna().check_is_admin(username)
      is_dokter = Pengguna().check_is_dokter(username)
      is_pasien = Pengguna().check_is_pasien(username)
      if is_admin:
        role='admin'
      elif is_dokter:
        role='dokter'
      elif is_pasien:
        role='pasien'
      else:
        role=''
      request.session['username'] = username
      request.session['password'] = password
      request.session['role'] = role
      return redirect('home')
  return render(request, 'login.html')

@login_required
def logout(request):
  request.session.delete()
  return redirect('authentication:login')

def register_admin(request):
  error = True
  if request.method == 'POST':
    username = request.POST['username']
    password = request.POST['password']
    nomor_id = request.POST['nomor_id']
    nama_lengkap = request.POST['nama_lengkap']
    tanggal_lahir = request.POST['tanggal_lahir']
    email = request.POST['email']
    alamat = request.POST['alamat']
    try:
      Pengguna().create_acc_admin(username, password, nomor_id, nama_lengkap, tanggal_lahir, email, alamat)
      request.session['username'] = username
      request.session['password'] = password
      request.session['role'] = 'admin'
      return redirect('home')
    except:
      error = True
  return render(request, 'register-admin.html', {'error': error})

def register_pasien(request):
  error = False
  if request.method == 'POST':
    username = request.POST['username']
    password = request.POST['password']
    nomor_id = request.POST['nomor_id']
    nama_lengkap = request.POST['nama_lengkap']
    tanggal_lahir = request.POST['tanggal_lahir']
    email = request.POST['email']
    alamat = request.POST['alamat']
    alergi = request.POST.getlist('alergi')
    try:
      Pengguna().create_acc_pasien(username, password, nomor_id, nama_lengkap, tanggal_lahir, email, alamat, alergi)
      request.session['username'] = username
      request.session['password'] = password
      request.session['role'] = 'pasien'
      return redirect('home')
    except:
      error = True
  return render(request, 'register-pasien.html', {'error': error})

def register_dokter(request):
  error = False
  if request.method == 'POST':
    username = request.POST['username']
    password = request.POST['password']
    nomor_id = request.POST['nomor_id']
    nama_lengkap = request.POST['nama_lengkap']
    tanggal_lahir = request.POST['tanggal_lahir']
    email = request.POST['email']
    alamat = request.POST['alamat']
    no_sip = request.POST['no_sip']
    spesialisasi = request.POST['spesialisasi']
    try:
      Pengguna().create_acc_dokter(username, password, nomor_id, nama_lengkap, tanggal_lahir, email, alamat, no_sip, spesialisasi)
      request.session['username'] = username
      request.session['password'] = password
      request.session['role'] = 'dokter'
      return redirect('home')
    except:
      error = True
  return render(request, 'register-dokter.html', {'error': error})

@login_required
def profile(request):
  username = request.session['username']
  password = request.session['password']
  pengguna = Pengguna().get_user(username, password)
  if pengguna == None:
    return redirect('home')
  pengguna['tanggal_lahir'] = str(pengguna['tanggal_lahir'])
  return render(request, 'profile.html', {'profil': pengguna})