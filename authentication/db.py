from django.db import connection
from medikago.serializers import serialize
import random
import string

class Pengguna():
  cursor = None

  def __init__(self):
    self.cursor = connection.cursor()
    self.cursor.execute("SET SEARCH_PATH TO medika_go")

  def get_user(self, username, password):
    self.cursor.execute("SELECT * FROM PENGGUNA WHERE username='{}' AND password='{}'".format(username, password))
    try:
      return serialize(self.cursor)[0]
    except IndexError:
      return None

  def check_is_admin(self, username):
    self.cursor.execute("SELECT * FROM ADMINISTRATOR WHERE username='{}'".format(username))
    if len(serialize(self.cursor)) != 0:
      return True
    return False

  def check_is_pasien(self, username):
    self.cursor.execute("SELECT * FROM PASIEN WHERE username='{}'".format(username))
    if len(serialize(self.cursor)) != 0:
      return True
    return False

  def check_is_dokter(self, username):
    self.cursor.execute("SELECT * FROM DOKTER WHERE username='{}'".format(username))
    if len(serialize(self.cursor)) != 0:
      return True
    return False

  def create_acc_admin(self, username, password, nomor_id, nama_lengkap, tanggal_lahir, email, alamat):
    self.cursor.execute("INSERT INTO PENGGUNA(username, password, nomor_id, nama_lengkap, tanggal_lahir, email, alamat) VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}' )".format(username, password, nomor_id, nama_lengkap, tanggal_lahir, email, alamat))
    self.cursor.execute("SELECT MAX(nomor_pegawai::int) FROM ADMINISTRATOR")
    last = 0
    for i in self.cursor:
      last = i[0]
    print(last)
    no_pegawai = last + 1
    self.cursor.execute("INSERT INTO ADMINISTRATOR(nomor_pegawai, username, kode_rs) VALUES ('{}', '{}', '{}')".format(no_pegawai, username, 1))

  def create_acc_dokter(self, username, password, nomor_id, nama_lengkap, tanggal_lahir, email, alamat, no_sip, spesialisasi):
    self.cursor.execute("INSERT INTO PENGGUNA(username, password, nomor_id, nama_lengkap, tanggal_lahir, email, alamat) VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}' )".format(username, password, nomor_id, nama_lengkap, tanggal_lahir, email, alamat))
    self.cursor.execute("SELECT MAX(id_dokter::int) FROM DOKTER")
    id_pegawai = 0
    for i in self.cursor:
      last = i[0]
    print(last)
    id_pegawai = last + 1
    self.cursor.execute("INSERT INTO DOKTER(id_dokter, username, no_sip, spesialisasi) VALUES ('{}', '{}', '{}','{}')".format(id_pegawai, username, no_sip, spesialisasi))

  def get_random_alphaNumeric_string(self, stringLength=15):
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join((random.choice(lettersAndDigits) for i in range(stringLength)))
  
  def create_acc_pasien(self, username, password, nomor_id, nama_lengkap, tanggal_lahir, email, alamat, alergi):
    self.cursor.execute("INSERT INTO PENGGUNA(username, password, nomor_id, nama_lengkap, tanggal_lahir, email, alamat) VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}' )".format(username, password, nomor_id, nama_lengkap, tanggal_lahir, email, alamat))
    no_rekam_medis = self.get_random_alphaNumeric_string()
    self.cursor.execute("INSERT INTO PASIEN(no_rekam_medis, username, nama_asuransi) VALUES ('{}', '{}', '{}')".format(no_rekam_medis, username, 'Allians'))
    for i in alergi:
      self.cursor.execute("INSERT INTO ALERGI_PASIEN(no_rekam_medis, alergi) VALUES ('{}', '{}') ".format(no_rekam_medis, i))