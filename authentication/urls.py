from django.urls import path
from . import views

app_name= 'authentication'
urlpatterns = [
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('register/admin', views.register_admin, name='registeradmin'),
    path('register/pasien', views.register_pasien, name='registerpasien'),
    path('register/dokter', views.register_dokter, name='registerdokter'),
]