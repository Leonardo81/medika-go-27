from django.shortcuts import redirect


def login_required(f):
    def wrap(request, *args, **kwargs):
        # this check the session if userid key exist, if not it will redirect to login page
        if request.session.session_key == None:
            return redirect("authentication:login")
        else:
            return f(request, *args, **kwargs)
    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap

def admin_permission(f):
    def wrap(request, *args, **kwargs):
        # this check the session role is admin, if not it will redirect to login page
        if request.session['role'] != 'admin':
            return redirect("home")
        else:
            return f(request, *args, **kwargs)
    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap
