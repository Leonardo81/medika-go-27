![alt text](static/image/MedikaGO.png)

# Medika Go 27

## TK Basis Data Kelompok 27 - Kelas C

* Christopher Y Hasian Tamba | 1806235681
* Leonardo | 1806191023
* Muhamad Faarih Ihsan | 1806191276
* Samuel Ludwig Ian | 1806191471

## Informasi penting Aplikasi

### URL aplikasi: 

<http://medika-go27.herokuapp.com/>

### Akses Database: 

- Via heroku CLI, use `heroku pg:psql postgresql-lively-46669 --app medika-go27`
- Set Schema: `set search_path to medika_go;`

### Akun Login Dummy:

- **Admin**
    - Username: **test_admin**
    - Password: **test12345**
- **Dokter**
    - Username: **test_dokter**
    - Password: **test12345**
- **Pasien**
    - Username: **test_pasien**
    - Password: **test12345**

