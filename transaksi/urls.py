from django.urls import path
from . import views

app_name= 'transaksi'
urlpatterns = [
    path('create/', views.create, name='create'),
    path('list/', views.lists, name='list'),
    path('list/<int:id_tr>', views.update, name='update'),
    path('delete/<int:id_tr>', views.delete, name='delete')
]