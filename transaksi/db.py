from django.db import connection
from medikago.serializers import serialize

class Transaksi():

  cursor = None

  def __init__(self):
    self.cursor = connection.cursor()
    self.cursor.execute("SET SEARCH_PATH TO medika_go")

  def get_all_transaksi(self):
    self.cursor.execute("SELECT * FROM TRANSAKSI ORDER BY id_transaksi::int")
    return serialize(self.cursor) 

  def get_transaksi(self, id_transaksi):
    self.cursor.execute("SELECT * FROM TRANSAKSI WHERE id_transaksi='{}' ".format(id_transaksi))
    try:
      return serialize(self.cursor)[0]
    except IndexError:
      return None

  def get_filter_transaksi_by_pasien_username(self, username):
    self.cursor.execute(
      "SELECT T.* FROM TRANSAKSI T, PASIEN P WHERE P.username='{}' and T.no_rekam_medis = P.no_rekam_medis"
      .format(username))
    return serialize(self.cursor)

  def create_transaksi(self, id_transaksi, datetime, no_rekam_medis):
    self.cursor.execute(
      "INSERT INTO TRANSAKSI (id_transaksi, tanggal, status, total_biaya, waktu_pembayaran, no_rekam_medis) VALUES ('{}','{}','{}',{},'{}','{}')"
      .format(id_transaksi, str(datetime.date()), 'Created', 0, str(datetime), no_rekam_medis)
    )

  def delete(self, id_transaksi):
    self.cursor.execute("DELETE FROM TRANSAKSI WHERE id_transaksi='{}'".format(id_transaksi))

  def update(self, id_transaksi, tanggal, status, waktu_bayar):
    self.cursor.execute(
      "UPDATE TRANSAKSI SET tanggal='{}', status='{}', waktu_pembayaran='{}' WHERE id_transaksi='{}'"
      .format(str(tanggal), status, str(waktu_bayar), id_transaksi)
    )

  def next_id(self):
    self.cursor.execute("SELECT coalesce(max(id_transaksi::INT), 0) as max FROM TRANSAKSI")
    max_id = int(serialize(self.cursor)[0]['max'])
    return max_id+1

  def get_no_rekam_medis(self):
    self.cursor.execute("SELECT no_rekam_medis, username FROM PASIEN")
    return serialize(self.cursor)