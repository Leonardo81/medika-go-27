from django.shortcuts import render, redirect
from .db import Transaksi
from authentication.auth import login_required, admin_permission
from datetime import datetime

@login_required
def lists(request):
  if request.session['role'] == 'pasien':
    username=request.session['username']
    list_transaksi = Transaksi().get_filter_transaksi_by_pasien_username(username=username)
  else: 
    list_transaksi = Transaksi().get_all_transaksi()

  return render(request, 'transaksi-list.html', {'list_transaksi':list_transaksi})

@login_required
@admin_permission
def create(request):
  error=False
  if request.method == 'POST':
    now = datetime.now()
    no_rekam_medis = request.POST['no_rekam_medis']
    id_tr = int(Transaksi().next_id())
    try:
      Transaksi().create_transaksi(id_tr, now, no_rekam_medis)
      return redirect('transaksi:list')
    except:
      error = True
  list_no_rekam_medis = Transaksi().get_no_rekam_medis()
  return render(request, 'create-transaksi.html', {'error': error, 'list_no_rekam_medis': list_no_rekam_medis})

@login_required
@admin_permission
def update(request, id_tr): 
  if request.method == "POST":
    tanggal = request.POST['tanggal']
    status = request.POST['status']
    waktu_pembayaran = request.POST['waktu_pembayaran']
    if len(waktu_pembayaran)<19 :
      waktu_pembayaran += ':00'
    waktu_pembayaran = datetime.strptime(waktu_pembayaran, '%Y-%m-%dT%H:%M:%S')
    print(tanggal, status, waktu_pembayaran)
    try:
      Transaksi().update(id_tr, tanggal, status, waktu_pembayaran)
      return redirect('transaksi:list')
    except:
      print('ERROR WOI')
  transaksi = Transaksi().get_transaksi(id_tr)
  transaksi['tanggal'] = str(transaksi['tanggal'])
  waktu_pembayaran = transaksi['waktu_pembayaran'].isoformat()
  transaksi['waktu_pembayaran'] = waktu_pembayaran.split('.')[0]
  return render(request, 'update-transaksi.html', transaksi)

@login_required
@admin_permission
def delete(request, id_tr):
  if request.method == "POST":
    try:
      Transaksi().delete(id_tr)
    except:
      print("ERROR WOI")
  return redirect("transaksi:list")
