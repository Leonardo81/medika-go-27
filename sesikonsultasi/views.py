from django.shortcuts import render, redirect
from .db import SesiKonsultasi
from authentication.auth import login_required, admin_permission

@login_required
@admin_permission
def add_sesi(request):
    if request.method == 'POST':
        no_rekam_medis = request.POST['no_rekam_medis']
        id_transaksi = request.POST['id_transaksi']
        tanggal = request.POST['tanggal']
        SesiKonsultasi().create_sesi_konsultasi(no_rekam_medis, id_transaksi, tanggal)
        return redirect("sesikonsultasi:daftar_sesi")
    else:
        list_transaksi = SesiKonsultasi().get_transaksi()
        list_no_rekam_medis = SesiKonsultasi().get_no_rekam_medis()
        return render(request, 'add_sesi.html', {'list_transaksi':list_transaksi, 'list_no_rekam_medis':list_no_rekam_medis})
        
@login_required
def daftar_sesi(request):
    if request.session['role'] == 'admin' :
        list_sesi_konsultasi = SesiKonsultasi().get_sesi_konsultasi_from_admin()
    elif request.session['role'] == 'pasien':
        list_sesi_konsultasi = SesiKonsultasi().get_sesi_konsultasi_from_pengguna(request.session['username'])
    else :
        list_sesi_konsultasi = SesiKonsultasi().get_sesi_konsultasi_from_dokter(request.session['username'])
    return render(request, 'list_sesi_konsultasi.html', {"list_sesi_konsultasi":list_sesi_konsultasi})

@login_required
@admin_permission
def update_sesi(request, idkonsultasi):
    if request.method == 'POST':
        tanggal = request.POST['tanggal']
        status = request.POST['status']
        SesiKonsultasi().update_detailed_sesi_konsultasi(idkonsultasi, tanggal, status)
        return redirect("sesikonsultasi:daftar_sesi")
    else :
        detail_sesi_konsultasi = SesiKonsultasi().get_detailed_sesi_konsultasi(idkonsultasi)
        detail = detail_sesi_konsultasi[0]
        tanggal = str(detail['tanggal'])
        return render(request, 'update_sesi_konsultasi.html', {'detail':detail, 'tanggal':tanggal})

@login_required
@admin_permission
def delete_sesi(request, idkonsultasi):
    if request.method == 'POST':
        SesiKonsultasi().delete(idkonsultasi)
    return redirect("sesikonsultasi:daftar_sesi")