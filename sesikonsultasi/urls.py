from django.urls import path
from . import views

app_name='sesikonsultasi'
urlpatterns = [
    path('add_sesi/', views.add_sesi, name='add_sesi'),
    path('daftar_sesi/', views.daftar_sesi, name='daftar_sesi'),
    path('daftar_sesi/<int:idkonsultasi>/', views.update_sesi, name='update_sesi'),
    path('daftar_sesi/delete_sesi/<int:idkonsultasi>/', views.delete_sesi, name='delete_sesi'),
]