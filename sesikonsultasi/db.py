from django.db import connection
from medikago.serializers import serialize


class SesiKonsultasi():
    cursor = None

    def __init__(self):
        self.cursor = connection.cursor()
        self.cursor.execute("SET SEARCH_PATH TO medika_go")

    def get_transaksi(self):
        self.cursor.execute("SELECT id_transaksi FROM TRANSAKSI")
        return serialize(self.cursor)
    
    def get_no_rekam_medis(self):
        self.cursor.execute("SELECT no_rekam_medis, username FROM PASIEN")
        return serialize(self.cursor)

    def create_sesi_konsultasi(self, no_rekam_medis, id_transaksi, tanggal):
        self.cursor.execute("SELECT id_konsultasi FROM SESI_KONSULTASI ORDER BY id_konsultasi DESC")
        last = 0
        for i in self.cursor:
            if (last < int(i[0])):
                last = int(i[0])
        id_konsultasi = int(last) + 1
        self.cursor.execute("INSERT INTO SESI_KONSULTASI(tanggal, biaya, status, no_rekam_medis_pasien, id_transaksi, id_konsultasi) VALUES ('{}', 0, 'BOOKED', '{}', '{}', '{}') ".format(tanggal, no_rekam_medis, id_transaksi, id_konsultasi))
        
    def get_sesi_konsultasi_from_admin(self):
        self.cursor.execute("SELECT * FROM SESI_KONSULTASI")
        return serialize(self.cursor)

    def get_sesi_konsultasi_from_pengguna(self, username):
        self.cursor.execute("SELECT s.tanggal, s.biaya, s.status, s.id_transaksi, s.id_konsultasi FROM SESI_KONSULTASI S JOIN PASIEN B ON s.no_rekam_medis_pasien = b.no_rekam_medis JOIN PENGGUNA AS A ON a.username = b.username WHERE a.username = '{}' ".format(username))
        return serialize(self.cursor)
    
    def get_sesi_konsultasi_from_dokter(self, username):
        self.cursor.execute("SELECT * FROM SESI_KONSULTASI")
        return serialize(self.cursor)

    def get_detailed_sesi_konsultasi(self, id_konsultasi):
        self.cursor.execute("SELECT * FROM SESI_KONSULTASI WHERE id_konsultasi  = '{}'".format(id_konsultasi))
        return serialize(self.cursor)

    def update_detailed_sesi_konsultasi(self, id_konsultasi, tanggal, status):
        self.cursor.execute("UPDATE SESI_KONSULTASI SET tanggal = '{}', status = '{}' where id_konsultasi = '{}'".format(tanggal, status, id_konsultasi))

    def delete(self, id_konsultasi):
        self.cursor.execute("DELETE FROM SESI_KONSULTASI WHERE id_konsultasi='{}'".format(id_konsultasi))
